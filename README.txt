                                                                     
-----------------------------------------------------------------
This modules removes the extra canvas area added by Drupal when image crop cache style is applied. 
This fix is applied in case actual image simensions are smaller than the cache preset dimensions.


Installation
-------------

* Copy the whole "no_image_canvas" directory to your modules
  directory - "sites/all/modules"
* Enable the module.

